#Programa creado por José Francisco Gallardo Ojeda
#para la asignatura Software para Ingeniería Electrónica.
#Esta rutina sirve para implementar la interfáz de usuario de un robot cocinero.

def main_setting():
    global opcion
    #Paso 1: Seleccione la receta que desea cocinar. (Se limitará a la opción 1.)
    print("\t.:MENÚ:.")
    print("1. Sopa de fideos para 2 personas.")
    print("2. Huevo duro.")
    print("3. Pastel de chocolate.")
    print("4. Salir.")
    print("")
    opcion=int(input("Digite una opción del menú: "))
    while opcion<1 or opcion>4:
        print("Digite una opción válida")
        opcion=int(input("Digite una opción del menú: "))
    if opcion==1:
        print("Ha seleccionado: sopa de fideos para 2 personas.")
        print(f" Inserte los siguientes ingredientes en el recipiente principal: \n 2 tazas de agua fria.")
        print(f" Inserte los siguientes ingredientes en el recipiente secundario: \n 1/2 taza de fideos para sopa. \n 1 caldito. ")
    else:
        while opcion!=1:
            print("Esta rutina se ha configurado para realizar la opción 1.")
            opcion=int(input("Digite una opción del menú: "))
    return True

def ingredientes_receta():
    global ing
    global ingredientes
    #Paso 2: Verificación de ingredientes en el robot.
    ing=False
    while ing==False:
        ingredientes=int(input(f"¿Añadió los ingredientes necesarios al robot? \n Afirmativo: 1. \n Negativo: 0. \n Digite una opción: "))
        if ingredientes==1:
            ing=True
        else:
            ing=False
            print("Agregue los ingredientes al robot.")
        return True

def inicio_de_coccion():
    from tqdm import tqdm
    from time import sleep
    global inicio
    #Paso 3: Dar inicio a la cocción.
    inicio=int(input("¿Desea dar inicio a la cocción?, el proceso completo demora 15 minutos. \n Afirmativo: 1. \n Negativo: 0. \n Digite una opción: "))
    if inicio==1:
        print("El proceso de cocción ha comenzado.")
        #El tiempo total dependerá de la marca de fideos y la capacidad de calentar los alimentos del robot, para fines de simulación esto se aceleró.
        #El progreso no es lineal.
        for i in tqdm(range(1,100),desc ="Progreso:"):
            sleep(.015)
        return True
    else:
        print("El robot se apagará, hasta pronto.")
        return False
